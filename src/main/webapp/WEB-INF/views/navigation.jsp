<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <spring:message code="project.title"/>
            </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="foodItem">
                <spring:message code="nav.view.item"/>
            </a></li>
            <li><a href="viewMealPlan">
                <spring:message code="nav.view.mealplan"/>
            </a></li>
            <li><a href="logout?logout=true">
                <spring:message code="nav.logout"/>
            </a></li>
        </ul>
    </div>
</nav>
