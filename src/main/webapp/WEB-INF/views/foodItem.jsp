<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <title><spring:message code="welcome.title"/></title>
</head>
<body>

<jsp:include page="navigation.jsp"/>

<div class="container">
    <div class="col-md-6">
        <form action="foodItem" method="post" modelAttribute="ItemList">
            <input name="delete" value="true" type="hidden">
            <table class="table table-striped">
                <h3><spring:message code="available.items.title"/></h3>
                <tr>
                    <th><spring:message code="delete.title"/></th>
                    <th>#</th>
                    <th><spring:message code="items.title"/></th>
                </tr>
                <c:set var="itemCount" value="0"/>
                <c:forEach items="${items}" var="item">
                    <tr>
                        <c:set var="itemCount" value="${itemCount + 1}"/>
                        <td><input type="checkbox" name="items[${itemCount}].name"
                                   value="${item.name}"></td>
                        <td><c:out value="${itemCount}"></c:out></td>
                        <td><c:out value="${item.name}"></c:out></td>
                    </tr>
                </c:forEach>
            </table>
            <button type="submit" class="btn btn-primary">
                <spring:message code="form.update.items"/>
            </button>
        </form>
        <br>
    </div>

    <div class="col-md-6">
        <springForm:form action="foodItem" method="post" commandName="item">
            <input name="insert" value="true" type="hidden">

            <h3><spring:message code="add.item.title"/></h3>

            <input name="name" type="text"/>
            <springForm:errors path="name" cssStyle="color: red"/><br>

            <button type="submit" class="btn btn-primary">
                <spring:message code="form.add"/>
            </button>
        </springForm:form>
    </div>
</div>
</body>
</html>