<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <title><spring:message code="welcome.title"/></title>
</head>
<body>
<div class="col-md-12">

    <div class="col-md-5"></div>

    <div class="col-md-2">
        <springForm:form class="form-group" method="post" action="login" commandName="user">

            <h3><spring:message code="welcome.title"/></h3>

            <h3><spring:message code="login.title"/></h3>

            <br><spring:message code="your.email"/><br>
            <input class="form-control" name="email" placeholder="Enter Email address" required>
            <springForm:errors path="email" cssStyle="color: red"/><br>

            <br><<spring:message code="your.password"/><br>
            <input class="form-control" type="password" name="password" placeholder="Enter Password" required>
            <springForm:errors path="password" cssStyle="color: red"/><br>

            <div style="color: red"><c:out value="${message}"></c:out></div>

            <button class="btn btn-success" type="submit">
                <spring:message code="form.login.submit"/>
            </button>
            <br>

            ​<a href="signUp">
            <spring:message code="new.user"/>
            </a>
        </springForm:form>
    </div>

</div>
</body>
</html>