<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <title>Welcome Home</title>
</head>
<body>

<jsp:include page="navigation.jsp"/>

<div class="container">
    <div class="col-md-12">

        <div class="col-md-6">
            <table class="table table-striped">
                <caption><h2>
                    <spring:message code="available.meal.title"/>
                </h2></caption>
                <tr>
                    <th><spring:message code="day.title"/></th>
                    <th><spring:message code="meal.type.title"/></th>
                    <th><spring:message code="items.title"/></th>
                    <th><spring:message code="option.title"/></th>
                </tr>
                <c:forEach items="${mealPlans}" var="meal">
                    <tr>
                        <td>
                            <label><c:out value="${meal.day}"></c:out></label>
                        </td>
                        <td>
                            <label><c:out value="${meal.type}"></c:out></label>
                        </td>
                        <td>
                            <c:forEach items="${meal.items}" var="item">
                                <label><c:out value="${item.name}"></c:out></label>
                            </c:forEach>
                        </td>
                        <td>
                            <form action="editMeal" method="post">
                                <input name="mealId" value="${meal.id}" type="hidden">
                                <button type="submit" class="btn btn-primary">
                                    <spring:message code="form.update"/>
                                </button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>

        <div class="col-md-6">
            <h3><spring:message code="edit.meal.title"/><br></h3>

            <div class="col-md-4">
                <form action="editMeal" method="post">
                    <input name="editMeal" value="true" type="hidden"/>

                    <h3><spring:message code="day.title"/></h3>
                    <select class="form-control" name="day">
                        <c:forEach items="${days}" var="day">
                            <option value="${day}"><c:out value="${day}"/></option>
                        </c:forEach>
                    </select>

                    <h3><spring:message code="meal.type.title"/></h3>
                    <select class="form-control" name="type">
                        <c:forEach items="${types}" var="type">
                            <option value="${type}"><c:out value="${type}"/></option>
                        </c:forEach>
                    </select>
                    <br><br>
                    <button type="submit" class="btn btn-primary">
                        <spring:message code="edit.title"/>
                    </button>
                </form>
            </div>
        </div>

    </div>
</div>
</body>
</html>