<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <title><spring:message code="welcome.title"/></title>
</head>

<body>

<jsp:include page="navigation.jsp"/>

<div class="col-md-2"></div>

<div class="col-md-8">
    <form action="updateMeal" method="post" modelAttribute="ItemList">
        <table class="table table-striped">

            <caption>
                <h2><spring:message code="edit.meal.title"/></h2>
            </caption>

            <tr>
                <th><spring:message code="day.title"/></th>
                <th><spring:message code="meal.type.title"/></th>
                <th><spring:message code="items.title"/></th>
            </tr>

            <tr>
                <c:set var="meal" value="${particularMeal}"/>
                <c:set var="itemCount" value="0"/>

                <td><label><c:out value="${meal.day}"></c:out></label></td>
                <td><label><c:out value="${meal.type}"></c:out></label></td>
                <td>
                    <input name="mealId" value="${meal.id}" type="hidden">

                    <c:forEach items="${meal.items}" var="item">
                        <label>
                            <input checked type="checkbox" name="items[${itemCount}].name" value="${item.name}"/>
                            <c:out value="${item.name}"></c:out><br>
                            <c:set var="itemCount" value="${itemCount + 1}"/>
                        </label>
                    </c:forEach>

                    <c:forEach items="${suggestItems}" var="item">
                        <label>
                            <input type="checkbox" name="items[${itemCount}].name" value="${item.name}"/>
                            <c:out value="${item.name}"></c:out><br>
                            <c:set var="itemCount" value="${itemCount + 1}"/>
                        </label>
                    </c:forEach><br>
                </td>
            </tr>
        </table>

        <div class="col-md-4"></div>

        <button type="submit" class="btn btn-primary">
            <spring:message code="form.submit.changes"/>
        </button>

    </form>
</div>
</body>
</html>