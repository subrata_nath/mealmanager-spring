package net.therap.mealmanager.Filter;

import net.therap.mealmanager.utils.Constant;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author subrata
 * @since 11/27/16
 */
@WebFilter(urlPatterns = {"/*"})
public class AuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException,
            IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession();

        if (canByPass(request)) {
            chain.doFilter(req, resp);
            return;
        }

        setNoCache(response);

        if (session.getAttribute(Constant.AUTHENTICATED_ATTRIBUTE) != null) {
            chain.doFilter(req, resp);
        } else {
            response.sendRedirect(request.getContextPath() + Constant.LOG_IN);
        }
    }

    @Override
    public void destroy() {

    }

    private boolean canByPass(HttpServletRequest request) {
        return request.getServletPath().startsWith(Constant.LOG_IN) ||
                request.getServletPath().startsWith(Constant.SIGN_UP) ||
                request.getServletPath().startsWith(Constant.ERROR) ||
                request.getServletPath().startsWith(Constant.LOG_OUT);
    }

    private void setNoCache(HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.setHeader("Pragma", "no-cache");
    }
}