package net.therap.mealmanager.utils;

import net.therap.mealmanager.domain.User;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpSession;

/**
 * @author subrata
 * @since 12/12/16
 */
@Component
public class SessionHelper {

    public void saveUserInfoInSession(HttpSession session, User user) {
        session.setAttribute(Constant.AUTHENTICATED_ATTRIBUTE, Constant.AUTHENTICATED_VALUE);
        session.setAttribute(Constant.USER_EMAIL_ATTRIBUTE, user.getEmail());
    }
}