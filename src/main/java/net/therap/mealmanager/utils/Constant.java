package net.therap.mealmanager.utils;

/**
 * @author subrata
 * @since 12/12/16
 */
public class Constant {

    public static final String AUTHENTICATED_ATTRIBUTE = "authenticated";
    public static final String USER_EMAIL_ATTRIBUTE = "email";
    public static final String AUTHENTICATED_VALUE = "true";

    public static final String LOG_IN = "/login";
    public static final String SIGN_UP = "/signUp";
    public static final String FOOD_ITEM = "/foodItem";
    public static final String EDIT_MEAL = "/editMeal";
    public static final String LOG_OUT = "/logout";
    public static final String UPDATE_MEAL = "/updateMeal";
    public static final String VIEW_MEAL_PLAN = "/viewMealPlan";
    public static final String ERROR = "/error";
}