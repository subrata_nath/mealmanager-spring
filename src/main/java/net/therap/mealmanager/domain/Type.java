package net.therap.mealmanager.domain;

/**
 * @author subrata
 * @since 11/22/16
 */
public enum Type {
    LUNCH, BREAKFAST
}