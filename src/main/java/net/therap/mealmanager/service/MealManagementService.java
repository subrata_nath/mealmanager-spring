package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.ItemDao;
import net.therap.mealmanager.dao.MealDao;
import net.therap.mealmanager.domain.Day;
import net.therap.mealmanager.domain.Item;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author subrata
 * @since 11/15/16
 */
@Service
@Transactional
public class MealManagementService {

    @Autowired
    private MealDao mealDao;

    @Autowired
    private ItemDao itemDao;

    public List<Day> getDays() {
        return Arrays.asList(Day.values());
    }

    public List<Type> getTypes() {
        return Arrays.asList(Type.values());
    }

    public int getMealIdFromDayAndType(Day day, Type type) {
        return mealDao.readMealId(day, type);
    }

    public List<Meal> showAllMealPlans() {
        return mealDao.readAllMealPlans();
    }

    public Meal getParticularMeal(int mealId) {
        return mealDao.readParticularMeal(mealId);
    }

    public List<Item> getExcludedItems(Meal meal) {

        List<Item> excludedItems = new ArrayList<>();
        List<Item> allItems = itemDao.readItems();

        for (Item item : allItems) {
            boolean included = false;

            for (Item includedItem : meal.getItems()) {
                if (item.getName().equals(includedItem.getName())) {
                    included = true;
                    break;
                }
            }

            if (!included) {
                excludedItems.add(item);
            }
        }

        return excludedItems;
    }

    public void updateMeal(int mealId, List<Item> items) {
        if (items != null) {
            mealDao.updateMeal(mealId, items);
        }else{
            mealDao.deleteMeal(mealId);
        }
    }
}