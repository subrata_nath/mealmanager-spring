package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.UserDao;
import net.therap.mealmanager.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

/**
 * @author subrata
 * @since 12/15/16
 */
@Service
@Transactional
public class UserService {

    @Autowired
    private UserDao userDao;

    public boolean addUser(User user) {
        if (userDao.isUserExisted(user)) {
            return false;
        } else {
            userDao.addUser(user);
            return true;
        }
    }

    public User getUser(User user) {
        return userDao.verifyLogin(user);
    }
}