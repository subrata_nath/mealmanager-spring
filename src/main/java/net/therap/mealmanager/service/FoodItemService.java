package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.ItemDao;
import net.therap.mealmanager.domain.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author subrata
 * @since 11/15/16
 */
@Service
@Transactional
public class FoodItemService {

    @Autowired
    private ItemDao itemDao;

    public List<Item> getAllItems() {
        return itemDao.readItems();
    }

    public void deleteItems(List<Item> items) {
        if (items != null) {
            itemDao.deleteItems(items);
        }
    }

    public void insertItem(Item item) {
        if (item.getName().trim().length() > 0) {
            itemDao.addItem(item);
        }
    }
}