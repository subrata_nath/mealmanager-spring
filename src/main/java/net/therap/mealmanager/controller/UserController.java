package net.therap.mealmanager.controller;

import net.therap.mealmanager.domain.User;
import net.therap.mealmanager.service.UserService;
import net.therap.mealmanager.utils.Constant;
import net.therap.mealmanager.utils.SessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Locale;

/**
 * @author subrata
 * @since 12/14/16
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private SessionHelper sessionHelper;

    @RequestMapping(value = Constant.LOG_IN, method = RequestMethod.GET)
    public String loginForm(Locale locale, HttpSession session) {

        if (session.getAttribute(Constant.AUTHENTICATED_ATTRIBUTE) != null) {
            return "redirect:" + Constant.FOOD_ITEM;
        }
        return Constant.LOG_IN;
    }

    @RequestMapping(value = Constant.LOG_IN, method = RequestMethod.POST)
    public String loginFormSubmit(Locale locale, HttpSession session, Model model, @Valid @ModelAttribute User user,
                                  BindingResult result) {
        if (result.hasErrors()) {
            model.addAttribute("user", user);
            return Constant.LOG_IN;
        }

        user = userService.getUser(user);
        if (user != null) {
            sessionHelper.saveUserInfoInSession(session, user);
            return "redirect:" + Constant.FOOD_ITEM;
        } else {
            model.addAttribute("message", "Wrong email/password");
            return Constant.LOG_IN;
        }
    }

    @RequestMapping(value = Constant.LOG_OUT)
    public String logOutForm(Locale locale, HttpSession session) {
        session.invalidate();
        return Constant.LOG_IN;
    }

    @RequestMapping(value = Constant.SIGN_UP, method = RequestMethod.GET)
    public String signUpForm(Locale locale, HttpSession session) {

        if (session.getAttribute(Constant.AUTHENTICATED_ATTRIBUTE) != null) {
            return "redirect:" + Constant.FOOD_ITEM;
        }
        return Constant.SIGN_UP;
    }

    @RequestMapping(value = Constant.SIGN_UP, method = RequestMethod.POST)
    public String signUpFormSubmit(Locale locale, HttpSession session, @RequestParam String confirmPassword, Model model,
                                   @Valid @ModelAttribute User user, BindingResult result) {
        if (result.hasErrors()) {
            model.addAttribute("user", user);
            return Constant.SIGN_UP;
        }

        if (!user.getPassword().equals(confirmPassword)) {
            model.addAttribute("message", "password mismatches!!");
            return Constant.SIGN_UP;
        } else if (userService.addUser(user)) {
            sessionHelper.saveUserInfoInSession(session, user);
            return "redirect:" + Constant.FOOD_ITEM;
        }

        model.addAttribute("message", "Email already exists!!");
        return Constant.SIGN_UP;
    }

    @RequestMapping(value = Constant.ERROR)
    public String notFound(Locale locale) {
        return Constant.ERROR;
    }

}