package net.therap.mealmanager.controller;

import net.therap.mealmanager.command.ItemList;
import net.therap.mealmanager.domain.Day;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.Type;
import net.therap.mealmanager.service.MealManagementService;
import net.therap.mealmanager.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Locale;

/**
 * @author subrata
 * @since 12/14/16
 */
@Controller
public class MealPlanController {

    @Autowired
    private MealManagementService mealManagementService;

    @RequestMapping(value = Constant.VIEW_MEAL_PLAN)
    public String viewMealPlan(Locale locale, Model model) {
        model.addAttribute("days", mealManagementService.getDays());
        model.addAttribute("types", mealManagementService.getTypes());
        model.addAttribute("mealPlans", mealManagementService.showAllMealPlans());
        return Constant.VIEW_MEAL_PLAN;
    }

    @RequestMapping(value = Constant.EDIT_MEAL, params = {"day", "type"}, method = RequestMethod.POST)
    public String getEditableMealByDayAndType(Locale locale, @RequestParam Day day, @RequestParam Type type,
                                              Model model) {
        int mealId = mealManagementService.getMealIdFromDayAndType(day, type);
        getEditableMealAndSetModel(mealId, model);
        return Constant.EDIT_MEAL;
    }

    @RequestMapping(value = Constant.EDIT_MEAL, params = "mealId", method = RequestMethod.POST)
    public String getEditableMealById(Locale locale, @RequestParam int mealId, Model model) {
        getEditableMealAndSetModel(mealId, model);
        return Constant.EDIT_MEAL;
    }

    @RequestMapping(value = {Constant.UPDATE_MEAL, Constant.EDIT_MEAL}, method = RequestMethod.GET)
    public String redirectToViewMealPlan(Locale locale) {
        return "redirect:" + Constant.VIEW_MEAL_PLAN;
    }

    @RequestMapping(value = Constant.UPDATE_MEAL, method = RequestMethod.POST)
    public String updateMeal(Locale locale, @ModelAttribute ItemList items, @RequestParam String mealId) {
        mealManagementService.updateMeal(Integer.parseInt(mealId), items.getItems());
        return "redirect:" + Constant.VIEW_MEAL_PLAN;
    }

    public void getEditableMealAndSetModel(int mealId, Model model) {
        Meal meal = mealManagementService.getParticularMeal(mealId);
        model.addAttribute("particularMeal", meal);
        model.addAttribute("suggestItems", mealManagementService.getExcludedItems(meal));
    }

}