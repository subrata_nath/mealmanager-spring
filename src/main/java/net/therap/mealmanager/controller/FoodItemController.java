package net.therap.mealmanager.controller;

import net.therap.mealmanager.command.ItemList;
import net.therap.mealmanager.domain.Item;
import net.therap.mealmanager.service.FoodItemService;
import net.therap.mealmanager.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Locale;

/**
 * @author subrata
 * @since 12/12/16
 */
@Controller
@RequestMapping(value = Constant.FOOD_ITEM)
public class FoodItemController {

    @Autowired
    private FoodItemService foodItemService;

    @RequestMapping(method = RequestMethod.GET)
    public String viewFoodItems(Locale locale, Model model) {
        model.addAttribute("items", foodItemService.getAllItems());
        return Constant.FOOD_ITEM;
    }

    @RequestMapping(params = "insert", method = RequestMethod.POST)
    public String insertNewItem(Locale locale, @Valid @ModelAttribute Item item, BindingResult result, Model model) {

        if (!result.hasErrors()) {
            foodItemService.insertItem(item);
        }

        model.addAttribute("items", foodItemService.getAllItems());
        return Constant.FOOD_ITEM;
    }

    @RequestMapping(params = "delete", method = RequestMethod.POST)
    public String deleteItems(Locale locale, @ModelAttribute ItemList items, Model model) {
        foodItemService.deleteItems(items.getItems());
        model.addAttribute("items", foodItemService.getAllItems());
        return Constant.FOOD_ITEM;
    }
}