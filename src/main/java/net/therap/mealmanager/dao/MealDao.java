package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.Day;
import net.therap.mealmanager.domain.Item;
import net.therap.mealmanager.domain.Meal;
import net.therap.mealmanager.domain.Type;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author subrata
 * @since 11/22/16
 */
@Repository
@SuppressWarnings("unchecked")
public class MealDao extends GenericDao<Meal> {

    public MealDao() {
        classType = Meal.class;
    }

    public List<Meal> readAllMealPlans() {
        List<Meal> meals = getSession().createCriteria(Meal.class).list();
        for (Meal meal : meals) {
            meal.getItems().size();
        }
        return meals;
    }

    public Meal readParticularMeal(int mealId) {
        Meal meal = (Meal) getSession().createCriteria(Meal.class)
                .add(Restrictions.eq("id", mealId)).uniqueResult();
        meal.getItems().size();
        return meal;
    }

    public void updateMeal(int id, List<Item> items) {

        Meal meal = (Meal) getSession().createCriteria(Meal.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
        Set<Item> itemSet = new HashSet<>();

        for (Item item : items) {
            item = (Item) getSession().createCriteria(Item.class)
                    .add(Restrictions.eq("name", item.getName())).uniqueResult();
            if (item != null) {
                itemSet.add(item);
            }
        }

        meal.setItems(itemSet);
        getSession().saveOrUpdate(meal);
    }

    public int readMealId(Day day, Type type) {

        Meal meal = (Meal) getSession().createCriteria(Meal.class)
                .add(Restrictions.eq("type", type))
                .add(Restrictions.eq("day", day))
                .uniqueResult();

        if (meal == null) {
            Meal newMeal = new Meal();
            newMeal.setType(type);
            newMeal.setDay(day);
            getSession().save(newMeal);
            return newMeal.getId();
        } else {
            return meal.getId();
        }
    }

    public void deleteMeal(int mealId) {
        Meal meal = (Meal) getSession().createCriteria(Meal.class)
                .add(Restrictions.eq("id", mealId))
                .uniqueResult();
        getSession().delete(meal);
    }
}