package net.therap.mealmanager.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * @author subrata
 * @since 11/22/16
 */
public abstract class GenericDao<T> {

    @Autowired
    private SessionFactory sessionFactory;

    public Class<T> classType;

    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public List<T> readTable() {
        return getSession().createCriteria(classType).list();
    }

    public void insertSingleEntity(T entity) {
        getSession().save(entity);
    }

    public void deleteSingleEntity(T entity) {
        getSession().delete(entity);
    }

}