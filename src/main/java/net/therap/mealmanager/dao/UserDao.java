package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.User;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * @author subrata
 * @since 11/28/16
 */
@Repository
public class UserDao extends GenericDao<User> {

    public UserDao() {
        classType = User.class;
    }

    public void addUser(User user) {
        insertSingleEntity(user);
    }

    public boolean isUserExisted(User user) {
        return getSession().createCriteria(User.class)
                .add(Restrictions.eq("email", user.getEmail()))
                .uniqueResult() != null;
    }

    public User verifyLogin(User user) {
        return (User) getSession().createCriteria(User.class)
                .add(Restrictions.eq("email", user.getEmail()))
                .add(Restrictions.eq("password", user.getPassword()))
                .uniqueResult();
    }
}