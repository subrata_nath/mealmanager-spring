package net.therap.mealmanager.dao;

import net.therap.mealmanager.domain.Item;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * @author subrata
 * @since 11/22/16
 */
@Repository
public class ItemDao extends GenericDao<Item> {

    public ItemDao() {
        classType = Item.class;
    }

    public void addItem(Item item) {
        if (getItemIfExists(item) == null) {
            insertSingleEntity(item);
        }
    }

    public void deleteItems(List<Item> items) {
        for (Item item : items) {
            item = getItemIfExists(item);
            if (item != null) {
                deleteSingleEntity(item);
            }
        }
    }

    public List<Item> readItems() {
        return readTable();
    }

    public Item getItemIfExists(Item item) {
        return (Item) getSession().createCriteria(Item.class)
                .add(Restrictions.eq("name", item.getName())).uniqueResult();
    }
}