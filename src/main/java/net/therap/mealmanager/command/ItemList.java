package net.therap.mealmanager.command;

import net.therap.mealmanager.domain.Item;
import java.util.List;

/**
 * @author subrata
 * @since 12/13/16
 */
public class ItemList {

    private List<Item> items;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}